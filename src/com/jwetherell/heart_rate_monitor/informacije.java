package com.jwetherell.heart_rate_monitor;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class informacije extends Activity{
	

	private static Button zgoButton;
    private static Button meritevButton;	
	
	private static final String TAG = "HeartRateMonitor";
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "info ");
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.info_view);
	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    TextView p1 = (TextView) findViewById(R.id.pomenmeritve1);
	    TextView p2 = (TextView) findViewById(R.id.pomenmeritve2);
	    TextView p3 = (TextView) findViewById(R.id.pomenmeritve3);
	    TextView p11 = (TextView) findViewById(R.id.pomenmeritve11);
	    TextView p21 = (TextView) findViewById(R.id.pomenmeritve22);
	    TextView p31 = (TextView) findViewById(R.id.pomenmeritve33);
	    
        Typeface f_light = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
        p1.setTypeface(f_light);
        p2.setTypeface(f_light);
        p3.setTypeface(f_light);
        p11.setTypeface(f_light);
        p21.setTypeface(f_light);
        p31.setTypeface(f_light);
	    
	    zgoButton = (Button) findViewById(R.id.zgoButton);
	    meritevButton = (Button) findViewById(R.id.merilnikB);
	    
	       zgoButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intentZgo = new Intent(informacije.this, zgodovina.class);
					startActivity(intentZgo);
					finish();
					
					
				}
			});
	        
	        meritevButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intentInfo = new Intent(informacije.this, HeartRateMonitor.class);
					finish();
					startActivity(intentInfo);
					
				}
			});
	       
	    }	    
	}

