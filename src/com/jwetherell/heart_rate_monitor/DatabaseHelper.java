package com.jwetherell.heart_rate_monitor;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper 
{

    public static final String TABLE_NAME = "vnosi";
    public static final String COLUMN_BPM ="bpm";
    public static final String COLUMN_STATUS ="status";
    public static final String COLUMN_OPOMBE ="opombe";
    public static final String COLUMN_DATUM ="datum";
    public static final String KEY_ID = "_id";
    
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";


    private static final String DATABASE_NAME = "trusted_phone.db";
    private static final int DATABASE_VERSION = 1;

    //create database
    private static final String DATABASE_CREATE = "create table " + TABLE_NAME + " ( " + 
    		BaseColumns._ID + " integer primary key autoincrement, " + 
    		COLUMN_BPM + " text not null," +
    		COLUMN_STATUS + TEXT_TYPE + COMMA_SEP +
    		COLUMN_OPOMBE + TEXT_TYPE + COMMA_SEP +
    		COLUMN_DATUM + TEXT_TYPE +
    		");";

    public DatabaseHelper(Context context) 
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        
    }

    @Override
    public void onCreate(SQLiteDatabase db) 
    {
        

        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
        
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
    }

}