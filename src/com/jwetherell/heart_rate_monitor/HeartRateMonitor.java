package com.jwetherell.heart_rate_monitor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicBoolean;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.text.Editable;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;



public class HeartRateMonitor extends Activity {

    private static final String TAG = "HeartRateMonitor";
    private static final AtomicBoolean processing = new AtomicBoolean(false);

    private static SurfaceView preview = null;
    private static SurfaceHolder previewHolder = null;
    private static Camera camera = null;
    private static View image = null;
    private static TextView text = null;
    private static ToggleButton vklop = null;
    private static TextView textMeritev = null;
    private static Chronometer meritev = null;
    private static WakeLock wakeLock = null;
    private static Button zgoButton;
    private static Button infoButton;
    
    private static int averageIndex = 0;
    private static final int averageArraySize = 4;
    private static final int[] averageArray = new int[averageArraySize];

    public static enum TYPE {
        GREEN, RED
    };

    private static TYPE currentType = TYPE.GREEN;

    public static TYPE getCurrent() {
        return currentType;
    }

    private static int beatsIndex = 0;
    private static final int beatsArraySize = 3;
    private static int[] beatsArray = new int[beatsArraySize];
    private static double beats = 0;
    private static long startTime = 0;
    private static long endTime = 0;
    private static int beatsAvg = 0;
    private static ArrayList<Integer> mSelectedItems = new ArrayList<Integer>();
    private static RadioGroup radioGroup;
    private static String rgID;
    private static EditText opp;
    private static String opombe;
    public static int abc;
    private static int counter = 0;
    
    


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        if(counter == 0){
        startDialog();
        counter ++;
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        zgoButton = (Button) findViewById(R.id.zgoButton);
        infoButton = (Button) findViewById(R.id.infoButton);
        vklop = (ToggleButton) findViewById(R.id.vklop);
        
        textMeritev = (TextView) findViewById(R.id.meritev);
        meritev = (Chronometer) findViewById(R.id.odstevanje);
        TextView bpm = (TextView) findViewById(R.id.bpmMain);
        
        meritev.setVisibility(View.INVISIBLE);
        final Button merButton = (Button) findViewById(R.id.merilnikB);
        preview = (SurfaceView) findViewById(R.id.preview);
        previewHolder = preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        image = findViewById(R.id.image);
        text = (TextView) findViewById(R.id.text);
        
        Typeface f_thin = Typeface.createFromAsset(getAssets(), "Roboto-Thin.ttf");
        Typeface f_light = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
        textMeritev.setTypeface(f_thin);
        meritev.setTypeface(f_thin);
        text.setTypeface(f_thin);
        bpm.setTypeface(f_light);
        vklop.setTypeface(f_thin);
        

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
 
        preview.setVisibility(View.GONE);
        
        text.setTextColor(Color.parseColor("#9A9A9A"));	
        vklop.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				if(vklop.isChecked()){
					beatsArray = new int[beatsArraySize];
			        beats = 0;
			        beatsAvg = 0;
		            
					wakeLock.acquire();

			        camera = Camera.open();

			        startTime = System.currentTimeMillis();
			        
			        preview.setVisibility(View.VISIBLE);
			        textMeritev.setText("...meritev poteka..");
			        meritev.setBase(SystemClock.elapsedRealtime());					
			        meritev.start();
			        meritev.setVisibility(View.VISIBLE);
			        //meritev.setText("...meritev poteka...");
			        text.setTextColor(Color.parseColor("#F6921E"));
			        zgoButton.setEnabled(false);
			        infoButton.setEnabled(false);
			        merButton.setEnabled(false);
				}
				else{					
					wakeLock.release();
			        camera.setPreviewCallback(null);
			        camera.stopPreview();
			        camera.release();
			        camera = null;
			        
			        zgoButton.setEnabled(true);
			        infoButton.setEnabled(true);
			        merButton.setEnabled(true);
			        
			        preview.setVisibility(View.GONE);
			        
			        endTime = startTime;
			        startTime = 0;
			        textMeritev.setText("");
			        
			        meritev.stop();
			        meritev.setVisibility(View.INVISIBLE);
			        int a = beatsAvg;
			        String dia = Integer.toString(a); 
			        text.setTextColor(Color.parseColor("#9A9A9A"));
			        
			        
			        
			        if(beatsAvg != 0){
			        	showDialog(dia);
			        }
			        
				}
			}
		});
        
        zgoButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intentZgo = new Intent(HeartRateMonitor.this, zgodovina.class);
				startActivity(intentZgo);
				
			}
		});
        
        infoButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intentInfo = new Intent(HeartRateMonitor.this, informacije.class);
				startActivity(intentInfo);
				
			}
		});
       
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private static PreviewCallback previewCallback = new PreviewCallback() {


        @Override
        public void onPreviewFrame(byte[] data, Camera cam) {
            if (data == null) throw new NullPointerException();
            Camera.Size size = cam.getParameters().getPreviewSize();
            if (size == null) throw new NullPointerException();

            if (!processing.compareAndSet(false, true)) return;

            int width = size.width;
            int height = size.height;

            int imgAvg = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(), height, width);
            // Log.i(TAG, "imgAvg="+imgAvg);
            if (imgAvg == 0 || imgAvg == 255) {
                processing.set(false);
                return;
            }

            int averageArrayAvg = 0;
            int averageArrayCnt = 0;
            for (int i = 0; i < averageArray.length; i++) {
                if (averageArray[i] > 0) {
                    averageArrayAvg += averageArray[i];
                    averageArrayCnt++;
                }
            }

            //izpusceno
            int rollingAverage = (averageArrayCnt > 0) ? (averageArrayAvg / averageArrayCnt) : 0;
            TYPE newType = currentType;
            if (imgAvg < rollingAverage) {
                newType = TYPE.RED;
                if (newType != currentType) {
                    beats++;
                    // Log.d(TAG, "BEAT!! beats="+beats);
                }
            } else if (imgAvg > rollingAverage) {
                newType = TYPE.GREEN;
            }

            if (averageIndex == averageArraySize) averageIndex = 0;
            averageArray[averageIndex] = imgAvg;
            averageIndex++;

            // Transitioned from one state to another to the same
            if (newType != currentType) {
                currentType = newType;
                image.postInvalidate();
            }

            long endTime = System.currentTimeMillis();
            double totalTimeInSecs = (endTime - startTime) / 1000d;
            if (totalTimeInSecs >= 10) {
                double bps = (beats / totalTimeInSecs);
                int dpm = (int) (bps * 60d);
                if (dpm < 30 || dpm > 180) {
                    startTime = System.currentTimeMillis();
                    beats = 0;
                    processing.set(false);
                    return;
                }

                // Log.d(TAG,
                // "totalTimeInSecs="+totalTimeInSecs+" beats="+beats);

                if (beatsIndex == beatsArraySize) beatsIndex = 0;
                beatsArray[beatsIndex] = dpm;
                beatsIndex++;

                int beatsArrayAvg = 0;
                int beatsArrayCnt = 0;
                for (int i = 0; i < beatsArray.length; i++) {
                    if (beatsArray[i] > 0) {
                        beatsArrayAvg += beatsArray[i];
                        beatsArrayCnt++;
                    }
                }
                beatsAvg = (beatsArrayAvg / beatsArrayCnt);
                text.setText(String.valueOf(beatsAvg));
                startTime = System.currentTimeMillis();
                beats = 0;
            }
            processing.set(false);
        }
    };

    private static SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {


        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(previewHolder);
                camera.setPreviewCallback(previewCallback);
            } catch (Throwable t) {
                Log.e("PreviewDemo-surfaceCallback", "Exception in setPreviewDisplay()", t);
            }
        }


        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            Camera.Size size = getSmallestPreviewSize(width, height, parameters);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
                Log.d(TAG, "Using width=" + size.width + " height=" + size.height);
            }
            camera.setParameters(parameters);
            camera.startPreview();
        }


        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // Ignore
        }
    };

    private static Camera.Size getSmallestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea < resultArea) result = size;
                }
            }
        }

        return result;
    }
    
   
	public void showDialog(String bpm){
    	
		text.setText("0");
    	AlertDialog.Builder builder = new AlertDialog.Builder(HeartRateMonitor.this);
    	final View v = getLayoutInflater().inflate(R.layout.dialog_view, null);
    	
    	builder.setView(v);
    	opp = (EditText)v.findViewById(R.id.opombe);
    	radioGroup = (RadioGroup)v.findViewById(R.id.rgroup);
    	
    	DatabaseHelper mDbHelper = new DatabaseHelper(getBaseContext());
    	final SQLiteDatabase db = mDbHelper.getWritableDatabase();

    	builder.setTitle("Vasa meritev je: "+ beatsAvg + " bpm");
    	builder.setPositiveButton("Shrani", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SimpleDateFormat df = new SimpleDateFormat("HH:mm   dd.MM.yyyy");
				
				String date = df.format(Calendar.getInstance().getTime());
				abc = radioGroup.getCheckedRadioButtonId();				
				opombe = opp.getText().toString();
				String status = getStatus();
				
				Intent zgoIntent = new Intent(HeartRateMonitor.this, zgodovina.class);
				
				Log.i("Opozorilo: ", String.valueOf(abc));
				Log.i("Opozorilo: ", String.valueOf(status));
				Log.i("Opozorilo: ", opombe);
				Log.i("Opozorilo: Ura je ==", String.valueOf(df));
				
				ContentValues values = new ContentValues();
				values.put(DatabaseHelper.COLUMN_BPM, String.valueOf(beatsAvg));
				values.put(DatabaseHelper.COLUMN_DATUM, date);
				values.put(DatabaseHelper.COLUMN_OPOMBE, opombe);
				values.put(DatabaseHelper.COLUMN_STATUS, status);
				db.insert(
						"vnosi", null,
				         values);
				db.close();
				startActivity(zgoIntent);
				dialog.dismiss();
			}
		});
    	
    	builder.setNegativeButton("Preklici", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
    	builder.create();
    	builder.show();
    	
    }
	
	public void startDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(HeartRateMonitor.this);
		builder.setTitle("Uvodnik v meritev");
		final View v = getLayoutInflater().inflate(R.layout.start_dialog_view, null);    	
    	builder.setView(v);
		builder.setPositiveButton("Vredu", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				
			}
		});
		builder.create().show();
	}
	
	public int getBPM(){
		return beatsAvg;
	}
	
	public String getStatus(){
		switch(abc){
		case R.id.aktivnost:
			return "aktivnost";
		case R.id.mirovanje:
			return "mirovanje";
		default:
			return "brez";
		}
	}
	
	public Editable getOpombe(){
		return opp.getText();
	}
	
}
