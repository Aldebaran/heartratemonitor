package com.jwetherell.heart_rate_monitor;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class zgodovina extends Activity {
	
	private static Button infoButton;
    private static Button meritevButton;
    private static TextView twDatum;
    private static int pos;
    private static String datumIzbris;
    
    private static ArrayList <String> datumUra = new ArrayList<String>();
    private static ArrayList <String>  stBPM = new ArrayList<String>();
    private static ArrayList <String> opombe = new ArrayList<String>();
    private static ArrayList <String> status = new ArrayList<String>();
    private static ArrayList <String> datumUra1 = new ArrayList<String>();
    private static ArrayList <String>  stBPM1 = new ArrayList<String>();
    private static ArrayList <String> opombe1 = new ArrayList<String>();
    private static ArrayList <String> status1 = new ArrayList<String>();
    private static ArrayList <String> id1 = new ArrayList<String>();
    private static ArrayList <String> id = new ArrayList<String>();
    
    private static int counter = 0;
    private static int mSizeDB;
    private static int mSizeDB1;
    private SimpleAdapter sa;
    ListView messages_list;
    DatabaseHelper mDbHelper;
    Cursor c;
    
    
    
	public void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.zgo);
	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    messages_list =  (ListView)findViewById(R.id.user_thread_lv); 
	    
	    
	    
	    mDbHelper = new DatabaseHelper(getBaseContext());
		final SQLiteDatabase db = mDbHelper.getReadableDatabase();
		
		String[] projection = {
				BaseColumns._ID,
				DatabaseHelper.COLUMN_BPM,
				DatabaseHelper.COLUMN_DATUM,
				DatabaseHelper.COLUMN_OPOMBE,
				DatabaseHelper.COLUMN_STATUS,
			    };
	
		c = db.query(DatabaseHelper.TABLE_NAME,
				projection, null, null, null, null, DatabaseHelper.KEY_ID + " DESC");
		
		if (counter == 0 ){
			if(c != null){
				if (c.moveToFirst()) {
					do{
						stBPM.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_BPM)));
						opombe.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_OPOMBE)));
						status.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_STATUS)));
						datumUra.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DATUM)));
						id.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.KEY_ID)));
					}while (c.moveToNext());
				} mDbHelper.close();
			}
			counter ++;
			mSizeDB = stBPM.size();
		}
		
		testDBsize();
		
		
		Log.i("Test: ", String.valueOf(stBPM.size()));
		Log.i("Test_nova: ", String.valueOf(mSizeDB1));
		
		changeDB();
		simpleArray();
	    Log.i("Izpis vnosov", stBPM + " "+ status + " " + opombe);	    
	  
	    infoButton = (Button) findViewById(R.id.infoButton);
	    meritevButton = (Button) findViewById(R.id.merilnikB);
	    
	    infoButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(zgodovina.this, informacije.class);
				startActivity(intent);
				
				finish();
			}
		});
	    meritevButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(zgodovina.this, HeartRateMonitor.class);
				startActivity(intent);
				finish();
				
			}
		});
	    
	    // Item Click Listener for the listview
	    OnItemLongClickListener itemClickListener = new OnItemLongClickListener() {
	        @Override
	        public boolean onItemLongClick(AdapterView<?> parent, View container, int position, long id) {
	        	//String a = (String) parent.getItemAtPosition(position);
	        	twDatum = (TextView) container.findViewById(R.id.idAdapter);
	        	datumIzbris = twDatum.getText().toString();
	            //Toast.makeText(getApplicationContext(), datumIzbris, Toast.LENGTH_LONG).show();
				izbrisDialog();
	        	return true;
	        }
	    };
	    messages_list.setOnItemLongClickListener(itemClickListener);
	    
	    messages_list.refreshDrawableState();
    
	}
	
	@Override
	public void onResume(){
		super.onResume();
		Log.i("Test: ", "Entered onRsume()");
	}
	
	public void onPause(){
		super.onPause();
		
	}
	
	
	 private void simpleArray(){
		 int[] flags = new int[]{
			        R.drawable.aktivnost,
			        R.drawable.mirovanje,
			        R.drawable.blank};
		 System.out.print(Arrays.toString(flags));
		 Log.i("Flags", Arrays.toString(flags));
		 String[] from = new String[] { "line1","line2", "line3", "line4", "line5" };
		 int[] to = new int[] {R.id.bpmAdapter, R.id.slikaAdapter, R.id.opombeAdapter, R.id.datumAdapter, R.id.idAdapter};
		 int mir; 
		 List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		  
		 for(int i = 0; i<stBPM.size(); i++){
			 String ak = "aktivnost";
			 String mi = "mirovanje";
			 Log.i("Status", status.get(i));
			 if((status.get(i)).equals(ak)){
				 mir = 0;
				 Log.i("Status: ", status.get(i) + String.valueOf(mir));
					HashMap<String,String> item;
			    	item = new HashMap<String,String>();
			    	item.put( "line1", stBPM.get(i));
			    	item.put( "line2", String.valueOf(flags[mir]));
			    	item.put( "line3", opombe.get(i));
			    	item.put("line4", datumUra.get(i));
			    	item.put("line5", id.get(i));
			    	list.add( item ); 			 }
			 else if((status.get(i)).equals(mi)){
				 mir = 1;
				 Log.i("Status: ", status.get(i) + String.valueOf(mir));
					HashMap<String,String> item;
			    	item = new HashMap<String,String>();
			    	item.put( "line1", stBPM.get(i));
			    	item.put( "line2", String.valueOf(flags[mir]));
			    	item.put( "line3", opombe.get(i));
			    	item.put("line4", datumUra.get(i));
			    	item.put("line5", id.get(i));
			    	list.add( item );
			 }
			 else {
			 
			 	//Log.i("Status: ", status.get(i) + String.valueOf(mir));
				HashMap<String,String> item;
		    	item = new HashMap<String,String>();
		    	item.put( "line1", stBPM.get(i));
		    	item.put( "line2", "");
		    	item.put( "line3", opombe.get(i));
		    	item.put("line4", datumUra.get(i));
		    	item.put("line5", id.get(i));
		    	list.add( item );
			 }
	    	sa = new SimpleAdapter(this, list, R.layout.adapter_view, from, to){
	    		public View getView(SimpleAdapter s, View convertView){
	    			View v = convertView;
	    			TextView dat = (TextView) findViewById(R.id.datumAdapter);
	    		    TextView bpm1 = (TextView) findViewById(R.id.bpmAdapter);
	    		    TextView bpm2 = (TextView) findViewById(R.id.bpmZapis);
	    		    TextView opp = (TextView) findViewById(R.id.opombeAdapter);
	    		    
	    		    Typeface f_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
	    	        Typeface f_light = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
	    	        dat.setTypeface(f_med);
	    	        bpm1.setTypeface(f_light);
	    	        bpm2.setTypeface(f_light); 
	    	        opp.setTypeface(f_light);
	    			return v;
	    		}
	    	};
	    	
			messages_list.setAdapter(sa);
			
			 
	        
		 }
		  
		 
		 }
	 
	 public void izbrisDialog(){
		 Log.i("Test - izbris", datumIzbris);
		 AlertDialog.Builder builder = new AlertDialog.Builder(zgodovina.this);
		 builder.setTitle("Izbris meritve");
		 builder.setMessage("Ali �elite res izbrisati izbrano meritev?");
		 
		 builder.setPositiveButton("Izbri�i", new DialogInterface.OnClickListener() {
			 
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					DatabaseHelper mDbHelper = new DatabaseHelper(getBaseContext());
			    	final SQLiteDatabase db = mDbHelper.getWritableDatabase();
			    	Log.i("klik", BaseColumns._ID + " pozicija " + pos);
			    	db.delete("vnosi", DatabaseHelper.KEY_ID + "= ? ", new String[] {datumIzbris});
			    	db.close();
			    	messages_list.refreshDrawableState();
			    	sa.notifyDataSetChanged();
			    	testDBsize();
			    	changeDB();
			    	simpleArray();
			    	dialog.dismiss();
			    	finish();
			    	startActivity(getIntent());
					
				}
			});
		 
		 builder.setNegativeButton("Prekli�i", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				
			}
		});
		 builder.create().show();
	 }
	 
	 public void testDBsize(){
		 mDbHelper.getReadableDatabase();
			if (c.moveToFirst()) {
				do{
					stBPM1.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_BPM)));
					opombe1.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_OPOMBE)));
					status1.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_STATUS)));
					datumUra1.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DATUM)));
					id1.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.KEY_ID)));
				}while (c.moveToNext());
			}
			mSizeDB1 = stBPM1.size();
			stBPM1 = new ArrayList<String>();
			opombe1 = new ArrayList<String>();
			status1 = new ArrayList<String>();
			datumUra1 = new ArrayList<String>();
			id1 = new ArrayList<String>();
			mDbHelper.close();
			
	 }
	 
	 public void changeDB(){
		 if(mSizeDB1 > mSizeDB || mSizeDB1 < mSizeDB){
				mDbHelper.getReadableDatabase();
				stBPM = new ArrayList<String>();
				opombe = new ArrayList<String>();
				status = new ArrayList<String>();
				datumUra = new ArrayList<String>();
				id = new ArrayList<String>();
				if (c.moveToFirst()) {
					do{
						stBPM.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_BPM)));
						opombe.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_OPOMBE)));
						status.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_STATUS)));
						datumUra.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DATUM)));
						id.add(c.getString(c.getColumnIndexOrThrow(DatabaseHelper.KEY_ID)));
					}while (c.moveToNext());
				} 
				mSizeDB = stBPM.size();
				mDbHelper.close();
			}
	 }
	
}
